#ifndef GENETICMOTIONPLANNING_H
#define GENETICMOTIONPLANNING_H

#include <vector>
#include <iostream>

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#include "AllegroWorld.h"
#include "geometrics.h"

int MIN_STEP = 10;
int MAX_STEP = 30;
float MAX_TETA = M_PI/8;

int MIN_PATH_SIZE = 100;
int MAX_PATH_SIZE = 500;

int POB_SIZE = 10;

float prob_cruce = 0.5;
float prob_mutacion = 0.4;
float generaciones = 50;

class GeneticMotionPlanning {
public:
    typedef std::vector<Point> Path;
    typedef std::vector<Point> Cromosome;


    AllegroWorld world;

    GeneticMotionPlanning(const AllegroWorld& _world) {
        world = _world;
    }

    Path plan() {
        clock_t t0 = clock();
        std::vector<Cromosome> pob = generateRandomPob();
        std::vector<Path> fenotipos;
        std::vector<float> aptitudes(POB_SIZE, 0.0f);

        int n_cruces = prob_cruce*POB_SIZE/2;
        int n_mutaciones = prob_mutacion*POB_SIZE;

        for (int it = 1; it <= generaciones; ++it) {
            fenotipos = decodePob(pob);
            for (int i = 0; i < pob.size(); ++i) {
                aptitudes[i] = 1.0/fitness(fenotipos[i]);
            }

            int best = getBestAptitude(aptitudes);
            std::cout << it << " " << aptitudes[best] << std::endl;
            printFitness(fenotipos[best]);

            if (it == generaciones) {
                cutPath(pob[best]);
                break;
            }

            std::vector<Cromosome> new_pob;
            // elitismo
            new_pob.push_back(pob[best]);

            // cruces
            for (int i = 0; i < n_cruces; ++i) {
                int p1 = selectByAptitude(aptitudes);
                int p2 = selectByAptitude(aptitudes);
                Cromosome c1, c2;
                cruce(pob[p1], pob[p2], c1, c2);
                new_pob.push_back(c1);
                new_pob.push_back(c2);
            }

            // mutacion
            for (int i = 0; i < n_mutaciones; ++i) {
                int idx = selectByAptitude(aptitudes);
                new_pob.push_back(mutate(pob[idx]));
            }


            // select new pob
            while (new_pob.size() < POB_SIZE) {
                new_pob.push_back(pob[selectByAptitude(aptitudes)]);
            }

            // recortes
            for (int i = 0; i < new_pob.size(); ++i) {
                new_pob[i] = cutPath(new_pob[i]);
                new_pob[i] = smoothPath(new_pob[i]);
                new_pob[i] = clearPath(new_pob[i]);
            }

            pob = new_pob;
        }

        int best = getBestAptitude(aptitudes);
        std::cout << "Time: " << (double)(clock() - t0)/CLOCKS_PER_SEC << std::endl;
        return decodeCromosome(pob[best]);
    }

private:
    float maxDeviationRange (float a, float b, float t) {
        float A = -1/(a+b);
        float B = -tan(M_PI/2 - t);
        float C = a*b/(a+b);
        float D = B*B - 4*A*C;
        float R = (-B - sqrt(D)) / (2*A);
        return R;
    }

    Cromosome genRandomCromosome() {
        Cromosome c;
        c.push_back(world.start);

        Point last_point = world.start;
        Line line_to_end (last_point, world.finish);
        while (collision(line_to_end, world.geos)) {
            Point new_point;
            Line last_line;
            do {
                // new_point = Point (last_point.x + uniform2()*MAX_STEP,
                //                    last_point.y + uniform2()*MAX_STEP);
                new_point = Point (world.window.x * uniform(),
                                   world.window.y * uniform());
                last_line = Line (last_point, new_point);
            } while (collision(last_line, world.geos) || !insideWindow(new_point, world.window));
            c.push_back(new_point);
            last_point = new_point;
            line_to_end = Line (last_point, world.finish);            
        }
        c.push_back(world.finish);
        return c;
    }

    Cromosome genRandomCromosome2() {
        Cromosome c;
        c.push_back(world.start);
        c.push_back(world.finish);
        int path_size = rand() % (MAX_PATH_SIZE - MIN_PATH_SIZE - 2) + MIN_PATH_SIZE;
        for (int i = 0; i < path_size; ++i) {
            int idx = rand() % (c.size() - 1);
            Point a = c[idx];
            Point b = c[idx+1];
            float d = distEuc(a, b);
            if (d < 2*MIN_STEP) {
                continue;
            }
            float x = MIN_STEP + uniform()*(d-2*MIN_STEP);
            float y = d - x;
            float t = uniform() * MAX_TETA;

            float z = (2*1 - 1) * maxDeviationRange(x, y, t);
            Point dif = b-a;
            float alpha = atan(dif.y/dif.x);
            float beta = atan(z/x);
            float gama = alpha + beta;

            float h = sqrt(x*x + z*z);
            Point nuevo = a + Point(h*cos(gama), h*sin(gama));

            c.insert(c.begin() + idx + 1, nuevo);
        }
        return c;
    }

    Path decodeCromosome(const Cromosome& c) {
        return c;
    }

    std::vector<Cromosome> generateRandomPob() {
        std::vector<Cromosome> pob;
        for (int i = 0; i < POB_SIZE; ++i) {
            pob.push_back(genRandomCromosome());
        }
        return pob;
    }

    std::vector<Path> decodePob(const std::vector<Cromosome>& pob) {
        std::vector<Path> ret;
        for (int i = 0; i < pob.size(); ++i) {
            ret.push_back(decodeCromosome(pob[i]));
        }
        return ret;
    }

    float fitness (const Path& path) {
        float tn = pathSize(path);
        float cols = collision(path, world.geos);
        float outs = countOutOfWindow(path, world.window);
        // smooth
        float smooth = 0;
        for (size_t i = 0; i < path.size()-2; ++i) {
            Point v = Line(path[i], path[i+1]).getVector(),
            w = Line(path[i+1], path[i+2]).getVector();
            float ang = v.angle(w);
            if (ang > MAX_TETA) {
                smooth += exp(10*(ang - MAX_TETA));
            }
        }
        // wrong steps
        float steps = 0;
        for (size_t i = 0; i < path.size()-1; ++i) {
            if (distEuc(path[i], path[i+1]) < MIN_STEP) {
                steps += 1;
            }
        }
        return tn + smooth + 500*steps;
    }

    void printFitness(const Path& path) {
        float tn = pathSize(path);
        float cols = collision(path, world.geos);
        float outs = countOutOfWindow(path, world.window);
        float smooth = 0;
        for (size_t i = 0; i < path.size()-2; ++i) {
            Point v = Line(path[i], path[i+1]).getVector(),
            w = Line(path[i+1], path[i+2]).getVector();
            float ang = v.angle(w);
            if (ang > MAX_TETA) {
                smooth += exp(10*(ang - MAX_TETA));
            }
        }
        // steps
        float steps = 0;
        for (size_t i = 0; i < path.size()-1; ++i) {
            if (distEuc(path[i], path[i+1]) < MIN_STEP) {
                steps += 1;
            }
        }
        std::cout << "Cols: " << cols << std::endl;
        std::cout << "Fitness: " << tn << " " << smooth << " " << steps << std::endl;
    }

    int getBestAptitude(std::vector<float>& aptitudes) {
        int best = 0;
        for (int i = 1; i < aptitudes.size(); ++i) {
            if (aptitudes[i] > aptitudes[best]) {
                best = i;
            }
        }
        return best;
    }

    float uniform() {
        return ((float) rand() / (RAND_MAX));
    }

    float uniform2() {
        return 2*uniform() - 1;
    }

    float sumAptitudes(const std::vector<float>& aptitudes) {
        float sum = 0;
        for (int i = 0; i < aptitudes.size(); ++i) {
            sum += aptitudes[i];
        }
        return sum;
    }

    int selectByAptitude(const std::vector<float>& aptitudes, float sum = -1) {
        if (sum == -1) {
            sum = sumAptitudes(aptitudes);
        }
        float r = uniform()*sum;
        float acum = 0;
        for (int i = 0; i < aptitudes.size(); ++i) {
            acum += aptitudes[i];
            if (acum >= r) {
                return i;
            }
        }
    }

    void cruce (const Cromosome& p1, const Cromosome& p2, Cromosome& c1, Cromosome& c2) {
        bool search_intersection = true;
        Line l1, l2;
        size_t i, j;
        Point p;
        for (i = 0; i < p1.size() - 1; ++i) {
            for (j = 0; j < p2.size() - 1; ++j) {
                l1 = Line (p1[i], p1[i+1]);
                l2 = Line (p2[j], p2[j+1]);
                if (l1.collidePoint(l2, p)) {
                    // child 1
                    c1 = Cromosome(p1.begin(), p1.begin() + i + 1);
                    c1.insert(c1.end(), p);
                    c1.insert(c1.end(), p2.begin() + j + 1, p2.end());
                    // child 2
                    c2 = Cromosome(p2.begin(), p2.begin() + j + 1);
                    c2.insert(c2.end(), p);
                    c2.insert(c2.end(), p1.begin() + i + 1, p1.end());
                    return;
                }
            }
        }
        c1 = p1;
        c2 = p2;
    }

    void cruce2 (const Cromosome& p1, const Cromosome& p2, Cromosome& c1, Cromosome& c2) {
        size_t i, j;
        // for (i = 1; i < p1.size()/2; ++i) {
        //     if (collision(Line(p1[i], p1[i+1]), world.geos)) {
        //         break;
        //     }
        // }
        // for (j = p2.size() - 2; j > p2.size()/2; --j) {
        //     if (collision(Line(p2[j], p2[j-1]), world.geos)) {
        //         break;
        //     }
        // }
        i = rand() % (p1.size()-2) + 1;
        j = rand() % (p2.size()-2) + 1;
        // child 1
        c1 = Cromosome(p1.begin(), p1.begin() + i);
        c1.insert(c1.end(), p2.begin() + j, p2.end());
        // child 2
        c2 = Cromosome(p2.begin(), p2.begin() + j);
        c2.insert(c2.end(), p1.begin() + i, p1.end());
    }

    Cromosome mutate (const Cromosome& crom) {
        Cromosome c = crom;
        size_t i = rand() % (c.size() - 2) + 1;

        Point new_point;
        Line l1;
        Line l2;
        int cont = 0;
        do {
            new_point = Point (world.window.x * uniform(),
                               world.window.y * uniform());
            l1 = Line (c[i-1], new_point);
            l2 = Line (new_point, c[i+1]);
            if (cont >= 1000) {
                return c;
            }
            cont++;
        } while (collision(l1, world.geos) ||
                 collision(l2, world.geos));
        c[i] = new_point;
        return c;
    }

    Cromosome mutate2 (const Cromosome& crom) {
        Cromosome c = crom;
        if (c.size() <= 1) {
            std::cout << "There are cromosomes of length 1" << std::endl;
        }
        int idx = rand() % (c.size() - 1);
        Point a = c[idx];
        Point b = c[idx+1];
        float d = distEuc(a, b);
        if (d < 2*MIN_STEP) {
            return c;
        }
        float x = MIN_STEP + uniform()*(d-2*MIN_STEP);
        float y = d - x;
        float t = uniform() * MAX_TETA;

        float z = (2*uniform() - 1) * maxDeviationRange(x, y, t);
        Point dif = a - b;
        float alpha = atan(dif.y/dif.x);
        float beta = atan(z/x);
        float gama = alpha + beta;

        float h = sqrt(x*x + z*z);
        Point nuevo = a + Point(h*cos(gama), h*sin(gama));

        c.insert(c.begin() + idx + 1, nuevo);
        return c;
    }

    bool cutPathAux(const Cromosome& crom, Cromosome& ret, int steps = 3) {
        ret = crom;
        Path path = decodeCromosome(ret);
        float vecindad = 20;
        int cuts = 1;
        while (cuts > 0) {
            cuts = 0;
            for (int i = 0; i < path.size(); ++i) {
                for (int j = path.size() - 1; j > i + steps; --j) {
                    if (distEuc(path[i], path[j]) < vecindad) {
                        cuts++;
                        Cromosome newCrom = Cromosome(ret.begin(), ret.begin() + i);
                        newCrom.insert(newCrom.end(), ret.begin() + j, ret.end());
                        ret = newCrom;
                        path = decodeCromosome(ret);
                        break;
                    }
                }
            }
        }
        if (ret.size() < 2) {
            return false;
        } else {
            return true;
        }
    }

    Cromosome cutPath (const Cromosome& crom) {
        Cromosome c = crom;
        Point p;
        Line l1, l2;
        for (size_t i = 0; i < c.size() - 1; ++i) {
            for (size_t j = c.size() - 2; j > i + 1; --j) {
                l1 = Line(c[i], c[i+1]);
                l2 = Line(c[j], c[j+1]);
                if (l1.collidePoint(l2, p)) {
                    Cromosome temp(c.begin(), c.begin() + i + 1);
                    temp.insert(temp.end(), p);
                    temp.insert(temp.end(), c.begin() + j + 1, c.end());
                    c = temp;
                    break;
                }
            }
        }
        return c;
    }

    Cromosome smoothPath (const Cromosome& crom) {
        Cromosome c = crom;
        for (size_t i = 0; i < c.size() - 2; ++i) {
            Point v = Line(c[i], c[i+1]).getVector(),
            w = Line(c[i+1], c[i+2]).getVector();
            float ang = v.angle(w);
            if (ang > MAX_TETA) {
                Point p, q;
                int cont = 0;
                do {
                    p = v*uniform() + c[i];
                    q = w*uniform() + c[i+1];
                    cont++;
                } while (cont < 100 && collision(Line(p, q), world.geos));
                if (cont < 100) {
                    c[i+1] = p;
                    c.insert(c.begin() + i + 2, q);
                }
            }
        }
        return c;
    }

    Cromosome clearPath (const Cromosome& crom) {
        Cromosome c = crom;
        for (size_t i = 0; i < c.size() - 2; ++i) {
            if (distEuc(c[i], c[i+1]) < MIN_STEP)
                if (!collision(Line(c[i], c[i+2]), world.geos)) {
                    c.erase(c.begin() + i + 1);
                } else {
                    // std::cout << "DEMONIOS" << std::endl;
                }
        }
        if (c.size() < 5) {
            std::cout << "SIzE " << crom.size() << " " <<c.size() << std::endl;
        }
        return c;
    }

    Cromosome cutPath2(const Cromosome& crom, bool debug = false) {
        Cromosome ret;
        if (debug) {
            std::cout << ret.size() << std::endl;
        }
        return ret;
    }
};

#endif
