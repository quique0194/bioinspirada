\documentclass[twocolumn]{article}

\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

\title{Algoritmo Genético para planificación de caminos de robots móviles}
\author{José Carrillo}
\date{}

\begin{document}

\maketitle

\begin{abstract}
La planificación de caminos consiste en generar una serie de puntos intermedios para llegar de un punto de partida a un punto de llegada. Para esto existen varios algoritmos deterministas pero que no toman en cuenta detalles como la suavidad del camino. En este artículo presentamos la construcción de un planificador de caminos genético que si toma en cuenta la suavidad del camino entre otros factores. Al final, realizaremos una comparación de nuestro algoritmo genético con el conocido A* para medir la calidad de los caminos resultantes y el tiempo de ejecución empleado.
\end{abstract}

\section{Introducción}
La planificación de caminos es un problema de robótica que consiste en lo siguiente. Dados dos puntos, uno de partido y otro de llegada, debemos encontrar una secuencia de puntos intermedios tal que el robot consiga ir del punto de partida al punto de llegada de forma óptima y sin colisionar con los obstáculos que se encuentran en el camino.
\\\\
Existen algoritmos deterministas que encuentran el camino óptimo, o simplemente prueban que no existe ningún camino del punto de partida al punto de llegada. Como el A*, sin embargo, estos algoritmos no tienen en cuenta detalles como la suvidad del camino, que puede ser determinante dependiendo de las restricciones que tiene el robot en su movimiento. Por ejemplo, un robot de cuatro ruedas describe curvas suaves dependiendo de se ángulo de giro.
\\\\
En este artículo se realizará la construcción de un planificador de caminos genético y posteriormente se realizará una comparación con un algoritmo determinista ampliamente utilizado: el A*. Los aspectos a consisderar serán la calidad de los caminos generados y el tiempo empleado en la generación de dichos caminos.

\section{Planificación genética}

\subsection{Representación y población inicial}
El cromosoma es una lista de tamaño variable, que contiene la secuencia de puntos (x, y) que conforman el camino. El primer elemento de la lista es el punto de partida y el último elemento es el punto de llegada. Dicho camino no debe colisionar con los obstáculos, y en general, a lo largo del algoritmo, no deben existir cromosomas que colisionen con los obstáculos.
\\\\
La población general debe ser generada aleatoriamente respetando las condiciones mencionadas antes. Para conseguir esto, creamos una lista vacía y agregamos el punto de partida, luego vamos agregando puntos aleatorios que no generen colisiones hasta que podamos unir directamente el último punto añadido con el punto de llegada sin generar colisiones. Si después de un número razonable de puntos en el camino, aún no se ha llegado al punto de llegada, podemos asumir que no existe un camino del punto de partida al punto de llegada y por lo tanto el algoritmo terminaría aquí.

\subsection{Mutación y Cruce}
El algoritmo de mutación usado, coge un punto dentro del cromosoma, que no sea ni el primero ni el último, y lo mueve de forma aleatoria sin generar una colisión con los obstáculos.
\\\\
El algoritmo de cruce, toma 2 cromosomas padres y devuelve 2 cromosomas hijos. Es un cruce simple de un punto que corta a ambos padres en el primer punto donde ambos caminos se cruzan. De esta manera se evita crear colisiones accidentales.

\subsection{Función de aptitud}
La función de aptitud toma en cuenta 3 cosas. En primer lugar la longitud del camino, en segundo lugar la suavidad de las curvas y en tercer lugar que los puntos intermedios no se encuentren tan juntos. Estos criterios se multiplican por constantes y se suman en forma de combinación lineal. El objetivo debe ser minimizar esta función.
\\\\
$eval(p) = w_d . dist(p) + w_s . suav(p) + w_c . simpl(p)$
\\\\
Donde $w_d$, $w_s$ y $w_c$ son constantes y $dist(p)$, $suav(p)$ y $simpl(p)$ están definidas de la siguiente forma:

\begin{itemize}
    \item $dist(p) = \sum_{i=1}^{n-1} {d(s_i)}$, donde $d(s_i)$ es la distancia entre 2 nodos adyacentes.
    \item $suav(p) = \sum_{i=2}^{n-1} {e^{\theta - \alpha}}$, donde $\theta$ es el ángulo que conecta los dos segmentos del punto $i$ y $\alpha$ es el máximo ángulo de giro permitido.
    \item $simpl(p) = \sum_{i=2}^{n} {cerca(s_i)}$, donde $cerca(s_i)$ retorna $1$ si el nodo $i$ está muy cerca del nodo $i-1$, o retorna $0$ en caso contrario.
\end{itemize}

\subsection{Correción de cromosomas}
Al ejecutar el algoritmo se notan algunos vicios, como que se generan ciclos innecesarios o que los ángulos de giro entre un segmento del camino y otro son muy grandes. También se ve una acumulación muy grande de puntos en ciertas zonas. Para solucionar cada estos 3 problemas, tenemos una función de corrección para cada uno.
\\\\
Para solucionar los ciclos innecesarios, tenemos una función que dado un cromosoma, busca los puntos donde el cromosoma se interseca consigo mismo, y elimina todo lo que hay entre la primera y la segunda vez que el cromosoma pasa por esa intersección. Ver figura (\ref{fig:eliminar_ciclos}).
\begin{figure}[htb]
\centering
\includegraphics[width=0.2\textwidth]{img/cut_path.jpg}
\caption{Eliminación de ciclos}
\label{fig:eliminar_ciclos}
\end{figure}
\\\\
Para suavizar las curvas y evitar que los ángulos de giro entre segmento y segmento sean demasiado grandes, tenemos una función de suavizado. Esta función toma un cromosoma y busca aquellos segmentos donde el ángulo de giro requerido supera un umbral preestablecido. En dichos segmentos agrega un segmento extra para dividir el ángulo de giro necesario entre 2 segmentos. Ver figura (\ref{fig:suavizar_camino}).
\begin{figure}[htb]
\centering
\includegraphics[width=0.2\textwidth]{img/smooth.jpg}
\caption{Suavización de camino}
\label{fig:suavizar_camino}
\end{figure}
\\\\
Finalmente, vemos que a consecuencia de la función de suavizado se generan muchos puntos muy juntos. Estos puntos efectivamente generan segmentos donde no hay un gran ángulo de giro, pero son segmentos tan cortos, que de igual manera el robot no podría ejecutar la maniobra. Para solucionar esto, recorremos el cromosoma punto por punto, y si encontramos dos puntos muy juntos, eliminamos el segundo punto y juntamos el primer punto con el punto que le seguía al segundo punto. Si generamos una colisión, deshacemos los cambios y dejamos esos puntos como estaban. De esta manera simplificamos el camino, eliminando los puntos innecesarios. Ver figura (\ref{fig:simplificar_camino}).
\begin{figure}[htb]
\centering
\includegraphics[width=0.2\textwidth]{img/simplify.jpg}
\caption{Simplificación de camino}
\label{fig:simplificar_camino}
\end{figure}

\section{Resultados}

A continuación se hará una comparación de los resultados de nuestro algoritmo genético con los resultados del ampliamente utilizado algoritmo de A*.
\\\\
Para llevar a cabo las pruebas, se usaron 4 mundos, todos de 500 x 500 pixeles. Estos mundos los puedes ver en las figuras (\ref{fig:caminos_astar} y \ref{fig:caminos_ga}) y tienen las siguientes características:

\begin{description}
    \item[Mundo 1] Hay un gran circulo que impide el paso directo del punto de partida al punto de llegada.
    \item[Mundo 2] Hay 2 rectángulos que obligan a que el camino tenga forma de zig-zag para llegar de un punto a otro.
    \item[Mundo 3] Hay 300 circulos pequeños que ocasionan que el camino del punto de partida al punto de llegada sea muy accidentado.
    \item[Mundo 4] Parecido al mundo 2, sólo que en lugar de tener 2 rectángulos, tenemos 4.
\end{description}

Los parámetros usados para el algoritmo genético fueron:

\begin{itemize}
    \item Probabilidad de cruce: 0.75
    \item Probabilidad de mutación: 0.2
    \item Generaciones: 50
    \item Individuos: 10
\end{itemize}

Para el A* se discretizó el espacio en cuadrados de lado 10 px.


\subsection{Caminos generados}
Se compararán los caminos resultantes de usar el algoritmo genético con los caminos resultantes de usar el A*, en cada uno de los cuatro mundos descritos anteriormente. Los caminos generados por el A* los podemos ver en la figura (\ref{fig:caminos_astar}), mientras que los caminos generados por el Algoritmo Genético, los apreciamos en la figura (\ref{fig:caminos_ga})

\begin{figure}[htb]
\centering
\includegraphics[width=0.4\textwidth]{img/astar.jpg}
\caption{Caminos generados por A*}
\label{fig:caminos_astar}
\end{figure}

\begin{figure}[htb]
\centering
\includegraphics[width=0.4\textwidth]{img/ga.jpg}
\caption{Caminos generados por Algoritmo Genético}
\label{fig:caminos_ga}
\end{figure}

\subsection{Tiempo de ejecución}

\begin{tabular}{| c | c | c |}
    \hline
    Mundo & Tiempo A* & Tiempo AG \\
    \hline
    Mundo 1 & 0.01'' & 0.02'' \\
    \hline
    Mundo 2 & 0.02'' & 0.15'' \\
    \hline
    Mundo 3 & 0.08'' & 2.2'' \\
    \hline
    Mundo 4 & 0.03'' & 0.6' \\
    \hline
\end{tabular}

\section{Conclusiones}
Vimos que es posible construir un planificador de caminos usando algoritmos genéticos. Y para verificar los resultados de nuestro planificador lo comparamos con el conocido A*.
\\\\
Los caminos resultantes fueron más suaves y más apropiados para el movimiento de robots con restricciones, por ejemplo, robots de cuatro ruedas. Sin embargo el tiempo de ejecución se vio notablemente incrementado, con lo que se restringe su uso a situaciones donde el tiempo de respuesta no sea crítico.
\\\\
Es importante notar que buena parte del tiempo utilizado por el algoritmo genético es utilizado por la función generadora de la población inicial, ya que tiene que buscar caminos aleatorios, pero que cumplan con las restricciones de comenzar en el punto de partida, terminar en el punto de llegada y no colisionar con los obstáculos.
\\\\
En trabajos futuros sería interesante mejorar la función inicializadora de la población aleatoria, de esta forma se incrementaría por lo menos el doble la eficiencia del algoritmo genético.


\begin{thebibliography}{X}

\bibitem{1} Gerke, M. (1999). Genetic path planning for mobile robots. In American Control Conference, 1999. Proceedings of the 1999 (Vol. 4, pp. 2424-2429). IEEE.

\bibitem{2} Tu, J., \& Yang, S. X. (2003, September). Genetic algorithm based path planning for a mobile robot. In Robotics and Automation, 2003. Proceedings. ICRA'03. IEEE International Conference on (Vol. 1, pp. 1221-1226). IEEE.

\bibitem{3} Ismail, A. T., Sheta, A., \& Al-Weshah, M. (2008). A mobile robot path planning using genetic algorithm in static environment. Journal of Computer Science, 4(4), 341-344.

\end{thebibliography}

\end{document}
