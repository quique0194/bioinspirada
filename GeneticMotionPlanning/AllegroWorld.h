#ifndef ALLEGRO_WORLD_H
#define ALLEGRO_WORLD_H

#include <fstream>
#include <vector>
#include <string>

#include "geometrics.h"

class AllegroWorld {
public:
    AllegroWorld (const char* filename = NULL) {
        if (filename) {
            std::cout << "asdf" << std::endl;
            load(filename);
        }
    }

    virtual ~AllegroWorld () {
        // for (size_t i = 0; i < geos.size(); ++i) {
        //     delete geos[i];
        // }
    }

    int load (const char* filename) {
        std::ifstream file(filename);
        std::string word;
        // read geos
        while (file >> word) {
            if (word == "window") {
                file >> window.x >> window.y;
                std::cout << window.x << std::endl;
            } else if (word == "rect") {
                Point a, b;
                file >> a.x >> a.y >> b.x >> b.y;
                geos.push_back(new Rect(a, b));
            } else if (word == "circle") {
                Point c;
                float r;
                file >> c.x >> c.y >> r;
                geos.push_back(new Circle(c, r));
            } else if (word == "start") {
                file >> start.x >> start.y;
            } else if (word == "finish") {
                file >> finish.x >> finish.y;
            } else {
                std::cout << "Error, element " << word << " not recognized" << std::endl;
                return -1;
            }
        }
        return 0;
    }

    void draw () {
        for (size_t i = 0; i < geos.size(); ++i) {
            geos[i]->draw();
        }
        start.draw();
        finish.draw();
    }

    std::vector<Geometry*> geos;
    Pointi window;
    Point start;
    Point finish;

};

#endif
