#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <vector>
#include <queue>
#include <iostream>
#include <map>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "geometrics.h"
#include "aStar.h"
#include "AllegroWorld.h"
#include "GeneticMotionPlanning.h"

#define NUMBER_OF_OBSTACLES 100

const float FPS = 60;


int main(int argc, char **argv){
    // srand(7);
    time_t seed = time(0);
    std::cout << "Seed: " << seed << std::endl;
    srand(seed);

    AllegroWorld world(argv[1]);

    GeneticMotionPlanning genetic_planner(world);
    std::vector<Point> path = genetic_planner.plan();

    // AStarPlanning astar_planner(world);
    // std::vector<Point> path = astar_planner.plan();


    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_TIMER *timer = NULL;
    bool redraw = true;

    if (!al_init()) {
        fprintf(stderr, "failed to initialize allegro!\n");
        return -1;
    }

    if (!al_init_primitives_addon()) {
        fprintf(stderr, "failed to initialize primitives!\n");
        return -1;
    }

    if (!al_install_mouse()) {
        fprintf(stderr, "failed to initialize the mouse!\n");
        return -1;
    }

    timer = al_create_timer(1.0 / FPS);
    if (!timer) {
        fprintf(stderr, "failed to create timer!\n");
        return -1;
    }

    display = al_create_display(world.window.x, world.window.y);
    if (!display) {
        fprintf(stderr, "failed to create display!\n");
        al_destroy_timer(timer);
        return -1;
    }

    event_queue = al_create_event_queue();
    if (!event_queue) {
        fprintf(stderr, "failed to create event_queue!\n");
        al_destroy_display(display);
        al_destroy_timer(timer);
        return -1;
    }

    al_register_event_source(event_queue, al_get_mouse_event_source());
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_clear_to_color(al_map_rgb(0,0,0));
    al_flip_display();
    al_start_timer(timer);


    while(1)
    {
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);

        if (ev.type == ALLEGRO_EVENT_TIMER) {
            redraw = true;
        } else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
                   ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {

        } else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {

        } else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
            
        } else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            break;
        }

        if (redraw && al_is_event_queue_empty(event_queue)) {
            redraw = false;
            al_clear_to_color(al_map_rgb(255,255,255));
            world.draw();
            draw_path(path);
            al_flip_display();
        }
    }

    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);

    return 0;
}