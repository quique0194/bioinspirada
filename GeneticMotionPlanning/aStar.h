#ifndef ASTAR_H
#define ASTAR_H

#include <time.h>
#include <iostream>
#include <vector>

#include "geometrics.h"
#include "AllegroWorld.h"

#define ASTAR_STEP 10


class AStarPlanning {
public:
    AllegroWorld world;

    AStarPlanning (const AllegroWorld& _world) {
        world = _world;
    }

    std::vector<Point> plan () {
        clock_t t0 = clock();
        std::priority_queue<Node> pq;   // 2 structures store the same for efficiency
        std::map<Point, bool> openSet;

        Node curNode(world.start, 0, distMan(world.start, world.finish));
        pq.push(curNode);

        std::map<Point, bool> visited;

        std::vector<Node> node_by_id;   // to reconstruct path


        while (distMan(curNode, world.finish) > ASTAR_STEP && visited.size() < 500000 && !pq.empty()) {
            curNode = pq.top();
            pq.pop();

            if (visited.count(curNode) > 0) {
                continue;
            }
            visited[curNode] = true;

            node_by_id.push_back(curNode);
            int parent_id = node_by_id.size() - 1;        

            std::vector<Node> nextNodes;
            // arriba, abajo, derecha, izquierda
            nextNodes.push_back(Node(curNode.x+ASTAR_STEP, curNode.y));
            nextNodes.push_back(Node(curNode.x-ASTAR_STEP, curNode.y));
            nextNodes.push_back(Node(curNode.x, curNode.y+ASTAR_STEP));
            nextNodes.push_back(Node(curNode.x, curNode.y-ASTAR_STEP));

            // diagonales
            nextNodes.push_back(Node(curNode.x+ASTAR_STEP, curNode.y+ASTAR_STEP));
            nextNodes.push_back(Node(curNode.x+ASTAR_STEP, curNode.y-ASTAR_STEP));
            nextNodes.push_back(Node(curNode.x-ASTAR_STEP, curNode.y+ASTAR_STEP));
            nextNodes.push_back(Node(curNode.x-ASTAR_STEP, curNode.y-ASTAR_STEP));

            for (int i = 0; i < nextNodes.size(); ++i) {
                if (!insideWindow(nextNodes[i], world.window) ||
                    visited.count(nextNodes[i]) > 0 ||
                    openSet.count(nextNodes[i]) > 0 ||
                    collision(Line(nextNodes[i], curNode), world.geos)) {
                        continue;
                }
                nextNodes[i].Gx = curNode.Gx + distMan(nextNodes[i], curNode);
                nextNodes[i].Hx = distMan(nextNodes[i], world.finish);
                nextNodes[i].parent_id = parent_id;
                pq.push(nextNodes[i]);
                openSet[nextNodes[i]] = true;
            }
        }
        std::cout << "Time: " << (double)(clock() - t0)/CLOCKS_PER_SEC << std::endl;
        if (distMan(curNode, world.finish) > ASTAR_STEP) {
            // path no found
            return std::vector<Point>();
        } else {
            // path found
            std::vector<Point> ret = reconstructPath(curNode, node_by_id);
            ret.push_back(world.finish);
            return ret;
        }
    }
private:
    struct Node: public Point {
        float Gx, Hx;
        int parent_id;
        bool operator<(const Node& n) const {
            return !(Gx + Hx < n.Gx + n.Hx);
        }
        Node(float _x = 0, float _y = 0): Point(_x, _y) {
            Gx = 0;
            Hx = 0;
            parent_id = -1;
        }
        Node(const Point& p, float _Gx = 0, float _Hx = 0): Point(p) {
            Gx = _Gx;
            Hx = _Hx;
            parent_id = -1;
        }
    };

    std::vector<Point> reconstructPath(const Node& n, const std::vector<Node>& node_by_id) {
        std::vector<Point> path;
        Node curNode = n;
        path.push_back(curNode);
        while (curNode.parent_id != -1) {
            curNode = node_by_id[curNode.parent_id];
            path.push_back(curNode);
        }
        return std::vector<Point>(path.rbegin(), path.rend());
    }
};

#endif
