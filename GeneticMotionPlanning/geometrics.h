#ifndef GEOMETRICS_H
#define GEOMETRICS_H

#include <iostream>

#include <math.h>
#include <algorithm>

template <class T> struct PointT;

typedef PointT<float> Pointf;
typedef Pointf Point;
typedef PointT<int> Pointi;

typedef std::vector<Point> Path;

struct Line;

struct Geometry {
    virtual void draw () = 0;
    virtual bool collide (const Line& line) {
        return false;
    }
    virtual int collide (const Path& path) {
        return 0;
    };
};

template <class T>
struct PointT : public Geometry {
    T x;
    T y;
    bool operator< (const PointT& p) const {
        if (x < p.x) {
            return true;
        } else if (x == p.x && y < p.y) {
            return true;
        } else {
            return false;
        }
    }
    PointT (T _x = 0, T _y = 0) {
        x = _x;
        y = _y;
    }
    PointT<T> operator+(const PointT<T>& p) const {
        return PointT<T>(x+p.x, y+p.y);
    }
    PointT<T> operator-(const PointT<T>& p) const {
        return PointT<T>(x-p.x, y-p.y);
    }
    PointT<T> operator*(float val) const {
        return PointT<T>(x*val, y*val);
    }
    PointT<T> operator/(float val) const {
        return PointT<T>(x/val, y/val);
    }
    bool operator== (const PointT<T>& p) const {
        return x == p.x && y == p.y;
    }
    float norm () const {
        return sqrt(x*x + y*y);
    }
    float dot (const PointT<T>& p) const {
        return x*p.x + y*p.y;
    }
    void draw () {
        al_draw_filled_circle(x, y, 1, al_map_rgb(0, 255, 255));
    }
    float cross (const PointT<T>& p) const {
        return (x*p.y) - (y*p.x);
    }
    float angle (const PointT<T>& p) const {
        if (norm() * p.norm() == 0) {
            return 0;
        }
        float x = dot(p)/(norm()*p.norm());
        if (x > 1) {
            x = 1;
        } else if (x < -1) {
            x = -1;
        }
        return acos(x);
    }
};


float distMan(const Point& a, const Point& b) {
    return fabs(a.x-b.x) + fabs(a.y-b.y);
}

float distEuc(const Point& a, const Point& b) {
    return sqrt(pow(a.x-b.x, 2) + pow(a.y-b.y, 2));
}



struct Line : public Geometry {
    Point a;
    Point b;
    Line () {}
    Line (Point _a, Point _b) {
        a = _a;
        b = _b;
    }
    void draw () {
        al_draw_line(a.x, a.y, b.x, b.y, al_map_rgb(255, 255, 0), 1);
    }

    Point projection (const Point& point) const {
        Point f = point - a;
        Point g = b - a;
        Point gu = g / g.norm();
        return a + gu * (f.dot(gu));
    }

    Point getVector () const {
        return Point(b.x - a.x, b.y - a.y);
    }

    bool collide (const Line& line) const {
        // http://stackoverflow.com/questions/7069420/check-if-two-line-segments-are-colliding-only-check-if-they-are-intersecting-n
        Point u = a - b;
        Point v = line.a - b;
        Point w = line.b - b;

        if (u.cross(v) * u.cross(w) > 0) {
            return false;
        }

        u = line.a - line.b;
        v = a - line.b;
        w = b - line.b;

        if (u.cross(v) * u.cross(w) > 0) {
            return false;
        }

        return true;
    }

    bool collidePoint (const Line& line, Point& p) const {
        // https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
        float x1 = a.x, y1 = a.y,
              x2 = b.x, y2 = b.y,
              x3 = line.a.x, y3 = line.a.y,
              x4 = line.b.x, y4 = line.b.y;
        float den = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
            
        if (fabs(den) <= 0.1) {
            return false;
        }
        float A = x1*y2 - y1*x2;
        float B = x3*y4 - y3*x4;

        float xnum = A*(x3-x4) - (x1-x2)*B;
        float ynum = A*(y3-y4) - (y1-y2)*B;

        p = Point (xnum/den, ynum/den);
        if (p.x >= std::min(x1, x2) && p.x <= std::max(x1, x2) &&
            p.x >= std::min(x3, x4) && p.x <= std::max(x3, x4)) {
            return true;
        }
        return false;
    }
};


struct Circle : public Geometry {
    Point center;
    float radius;
    Circle (float _x = 0, float _y = 0, float _radius = 10): center(_x, _y) {
        radius = _radius;
    }
    Circle (const Point& _center, float _radius) {
        center = _center;
        radius = _radius;
    }
    void draw () {
        al_draw_filled_circle(center.x, center.y, radius, al_map_rgb(255, 0, 0));
    }
    bool collide (const Point& point) {
        return distEuc(point, center) <= radius;
    }

    bool collide (const Line& line) {
        Point p = line.projection(center);
        if (collide(line.a) ||
            collide(line.b)) {
                return true;
        }
        if (p.x > std::max(line.a.x, line.b.x) ||
            p.x < std::min(line.a.x, line.b.x) ||
            p.y > std::max(line.a.y, line.b.y) ||
            p.y < std::min(line.a.y, line.b.y)) {
                return false;
        }
        return collide(p);
    }
    int collide (const Path& path) {
        int count = 0;
        for (size_t i = 1; i < path.size(); ++i) {
            if (collide(Line(path[i-1], path[i]))) {
                count++;
            }
        }
        return count;
    }
};

struct Rect : public Geometry {
    Point a;
    Point b;
    Rect () {}
    Rect (Point _a, Point _b) {
        a = Point(std::min(_a.x, _b.x), std::min(_a.y, _b.y));
        b = Point(std::max(_a.x, _b.x), std::max(_a.y, _b.y));
    }
    void draw () {
        al_draw_filled_rectangle(a.x, a.y, b.x, b.y, al_map_rgb(255, 0, 0));
    }
    bool is_inside (const Point& p) {
        return  p.x < b.x && p.x > a.x &&
                p.y < b.y && p.y > a.y;
    }
    std::vector<Point> getCorners () {
        std::vector<Point> v;
        v.push_back(a);
        v.push_back(Point(a.x, b.y));
        v.push_back(b);
        v.push_back(Point(b.x, a.y));
        return v;
    }
    bool collide (const Line& line) {
        std::vector<Point> v = getCorners();
        return is_inside(line.a) || is_inside(line.b) ||
               line.collide(Line(v[0], v[1])) ||
               line.collide(Line(v[1], v[2])) ||
               line.collide(Line(v[2], v[3])) ||
               line.collide(Line(v[3], v[0]));
    }
    int collide (const Path& path) {
        int count = 0;
        for (size_t i = 1; i < path.size(); ++i) {
            if (collide(Line(path[i-1], path[i]))) {
                count++;
            }
        }
        return count;
    }
};



bool insideWindow(const Point& p, const Pointi& window) {
    return p.x <= window.x && p.x >= 0 &&
           p.y <= window.y && p.y >= 0;
}

int countOutOfWindow(const Path& path, const Pointi& window) {
    int count = 0;
    for (size_t i = 0; i < path.size(); ++i) {
        if (!insideWindow(path[i], window)) {
            count++;
        }
    }
    return count;
}


// Path


void draw_path (const Path& path) {
    for (int i = 1; i < path.size(); ++i) {
        al_draw_filled_circle(path[i].x, path[i].y, 1, al_map_rgb(0, 255, 255));
        al_draw_line(path[i].x, path[i].y, path[i-1].x, path[i-1].y, al_map_rgb(0, 0, 0), 3);
    }
    
}



int get_nearest(const Point& p, const std::vector<Circle>& circles) {
    int min_idx = 0;
    float min_dist = 99999;
    for (int i = 1; i < circles.size(); ++i) {
        float dist = distEuc(p, circles[i].center);
        if (dist < min_dist) {
            min_idx = i;
            min_dist = dist;
        }
    }
    return min_idx;
}

Point generateRandomPoint(const Pointi& window) {
    return Point(rand() % window.x,
                 rand() % window.y);
}

// Point generateFreeRandomPoint(const std::vector<Circle>& obstacles,
//                               const Pointi& window) {
//     Point p = generateRandomPoint(window);
//     while (collide<Point>(p, obstacles)) {
//         p = generateRandomPoint(window);
//     }
//     return p;
// }

std::vector<Circle> generateObstacles(int count, const Pointi& window) {
    std::vector<Circle> circles(count, Circle());
    for (int i = 0; i < count; ++i) {
        circles[i].center = generateRandomPoint(window);
    }
    return circles;
}

bool collision (const Line& line, const std::vector<Geometry*>& geos) {
    for (size_t i = 0; i < geos.size(); ++i) {
        if (geos[i]->collide(line)) {
            return true;
        }
    }
    return false;
}

int collision (const Path& path, const std::vector<Geometry*>& geos) {
    int count = 0;
    for (size_t i = 0; i < geos.size(); ++i) {
        count += geos[i]->collide(path);
    }
    return count;
}

float pathSize (const Path& path) {
    float ret = 0;
    for (int i = 1; i < path.size(); ++i) {
        ret += distEuc(path[i], path[i-1]);
    }
    return ret;
}

// COUTS


std::ostream& operator<< (std::ostream &os, const Point& p) {
    return os << "Point(" << p.x << ", " << p.y << ")";
}


std::ostream& operator<< (std::ostream &os, const Line& l) {
    return os << "Line[" << l.a << "; " << l.b << "]";
}

#endif
